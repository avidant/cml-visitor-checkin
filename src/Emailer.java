/*
 * This program was written by and belongs to Avidant Bhagat.
 * No part of this proram may be edited, reproduced, or used without written
 * permission of Avidant Bhagat.
 */


import java.io.UnsupportedEncodingException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author avidant
 * Copyright Avidant Bhagat. No part of this code may be used without prior
 * written permission of Avidant Bhagat(avidant@uw.edu).
 */
public class Emailer {
    
    private final String username = "";
    private final String password = "";
    private final String name = "";
    private Properties sendProps;
    private Session sendSession;
    private Properties readProps;
    private Session readSession;
    private Folder folder;
    private final String receivingHost = "imap.googlemail.com";

    /**
     * Constructs Emailer object for cmbot@uw.edu
     * @throws NoSuchProviderException
     * @throws MessagingException
     */
    public Emailer() throws NoSuchProviderException, MessagingException {
        sendProps = new Properties();
        sendProps.put("mail.smtp.auth", "true");
        sendProps.put("mail.smtp.starttls.enable", "true");
        sendProps.put("mail.smtp.host", "smtp.gmail.com");
        sendProps.put("mail.smtp.port", "587");

        sendSession = Session.getInstance(sendProps,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
          });
        readProps = System.getProperties();
        readProps.setProperty("mail.store.protocol", "imaps");
        readSession = Session.getDefaultInstance(readProps, null);
        Store store = readSession.getStore("imaps");
        store.connect(receivingHost,username, password);
        folder = store.getFolder("INBOX");
        folder.open(Folder.READ_WRITE);
    }
    
    /**
     * Sends an email to using the given receiver's email address, subject and text
     * @param to is the receiver's email address as a String
     * @param subject is the subject of the email as a String
     * @param text is the body of the email as a String
     * @throws MessagingException
     * @throws UnsupportedEncodingException 
     */
    public void sendEmail(String to, String subject, String text) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(sendSession);
        message.setFrom(new InternetAddress(username, name));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
        message.setSubject(subject);
        message.setText(text);
        Transport.send(message);
    }
    
    /**
     * Reads the messages in the inbox
     * @return Array of messages in the inbox
     * @throws MessagingException 
     */
    public Message[] readEmail() throws MessagingException {
        return folder.getMessages();
    }
}
